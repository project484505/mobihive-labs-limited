from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from .models import DocumentSet, Customer, CustomerDocument, DocumentSetModel, CustomerModel
from .forms import CustomerForm


@login_required
def onboarding_view(request):
    document_sets = DocumentSet.objects.all()
    if request.method == 'POST':
        form = CustomerForm(request.POST, request.FILES)
        if form.is_valid():
            customer = form.save(commit=False)
            customer.created_by = request.user
            customer.save()
            document = CustomerDocument(customer=customer, attached_file=request.FILES['attached_file'])
            document.save()
            # Trigger AWS Textract or Google Cloud Vision API for data extraction
            extracted_json = extract_data_from_document(document.attached_file.path)
            document.extracted_json = extracted_json
            document.save()
            return redirect('onboarding:onboarding_view')
    else:
        form = CustomerForm()
    return render(request, 'onboarding/onboarding.html', {'document_sets': document_sets, 'form': form})


def extract_data_from_document(file_path):
    # Implement AWS Textract or Google Cloud Vision API data extraction here
    pass


def document_set_view(request):
    document_sets = DocumentSet.objects.all()
    return render(request, 'document_set.html', {'document_sets': document_sets})


def customer_view(request):
    customers = Customer.objects.all()
    return render(request, 'customer.html', {'customers': customers})


def customer_document_view(request):
    customer_documents = CustomerDocument.objects.all()
    return render(request, 'customer_document.html', {'customer_documents': customer_documents})


def document_set_model_view(request):
    document_set_models = DocumentSetModel.objects.all()
    return render(request, 'document_set_model.html', {'document_set_models': document_set_models})


def customer_model_view(request):
    customer_models = CustomerModel.objects.all()
