from django import forms
from django.contrib.auth.models import User
from .models import CountryModel, DocumentSetModel, CustomerModel, CustomerDocumentModel
# from customer import forms


class ModelForm:
    pass


class CustomerForm(forms.ModelForm):
    """
    Form to create a new customer.
    """

    class Meta:
        model = CustomerModel
        fields = ('last_name', 'first_name', 'nationality', 'gender')

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.fields['nationality'].queryset = CountryModel.objects.filter(id__in=user.country.id)


class DocumentForm(forms.ModelForm):
    """
    Form to upload a document.
    """

    class Meta:
        model = CustomerDocumentModel
        fields = ('attached_file',)

    def __init__(self, user, document_set, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.document_set = document_set

    def clean_attached_file(self):
        attached_file = self.cleaned_data['attached_file']
        if not attached_file.name.endswith(('.pdf', '.jpg', '.jpeg', '.png')):
            raise forms.ValidationError('Only PDF, JPG, JPEG, and PNG files are allowed.')
        return attached_file


class CreateCustomerForm(forms.Form):
    """
    Form to select a document set and upload a document.
    """
    document_set = forms.ModelChoiceField(queryset=DocumentSetModel.objects.none())
    attached_file = forms.FileField()

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.fields['document_set'].queryset = DocumentSetModel.objects.filter(country=user.country)


class EmailField:
    pass
