from django.contrib.auth.forms import UserCreationForm
from django.contrib.gis.geoip2.resources import Country
from django.db import models
from django.contrib.auth.models import User
# from django.forms.formsets.BaseFormSet import forms
from django.contrib.auth.forms import UserCreationForm


class Country(models.Model):
    name = models.CharField(max_length=100)


class City(models.Model):
    name = models.CharField(max_length=100)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)


class CountryModel(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class DocumentSet(models.Model):
    def __init__(self, *args, **kwargs):
        super().__init__(args, kwargs)
        self.last_name = None
        self.first_name = None

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    nationality = models.ForeignKey(Country, on_delete=models.CASCADE)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class CustomerDocument(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    attached_file = models.FileField(upload_to='customer_documents/')
    extracted_json = models.JSONField(default=dict)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Document for {self.customer}"


class DocumentSetModel(models.Model):
    name = models.CharField(max_length=50)
    country = models.ManyToManyField(Country)
    has_backside = models.BooleanField(default=False)
    ocr_labels = models.JSONField(default=dict)

    def __str__(self):
        return self.name


class CustomerModel(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    nationality = models.ForeignKey(Country, on_delete=models.CASCADE)
    gender = models.CharField(max_length=10)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class CustomerDocumentModel:
    pass


class CustomUserCreationForm(UserCreationForm):
    # email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(CustomUserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user
