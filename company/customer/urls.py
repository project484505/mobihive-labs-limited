from django.urls import path
from . import views

app_name = 'customer'

urlpatterns = [
    path('onboarding/', views.onboarding_view, name='onboarding_view'),
    path('customer/',views.CustomerModel,name='customerModel'),
]