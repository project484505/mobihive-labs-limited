from django.contrib import admin
from .models import CustomerModel


class YourModelAdmin(admin.ModelAdmin):
    def your_custom_method(self, request, queryset):
        for obj in queryset:
            # Accessing the model's options
            opts = obj._meta
            # Do something with opts
            pass

    your_custom_method.short_description = "Your Custom Method"


admin.site.register(CustomerModel, CustomerModel)
